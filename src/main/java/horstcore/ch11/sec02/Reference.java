package horstcore.ch11.sec02;

public @interface Reference {
    long id();
}