package urma.behaviorParam;

/**
 * Created by Adam on 7/15/2015.
 */

import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BehaviorP1 {

    public static void main(String[] args) {


        Runnable runner = new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        };


        new Thread(runner).start();


       new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Toolkit.getDefaultToolkit().beep();
            }
        }).start();




    }
}
